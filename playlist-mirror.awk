#!/usr/bin/awk -f

# Copyleft (c) Pierre-Jean Turpeau 02/2022 
# <pierrejean AT turpeau DOT net>

BEGIN {
	files_count = 0;
	error = 0
	m4p_count = 0
}

{
	if( match($0, "^#.*") ) {
		lines_array[lines_count] = $0;
	} else {
		#print $0;
		filename = cygwin_path($0);

		# retrieve music file extension and check it's not protected (.m4p)
		extension = substr(filename, length(filename) - 3)
		if( extension == ".m4p" ) {
			rel_path = relative_path(filename)
			dir_name = dirname(rel_path)

			print "  ** PROTECTED FILE FOUND ("files_count"): ", basename(filename) "\t\t" dir_name
			error = 1
			m4p_count++
		}

		files_array[files_count] = filename
		files_count++
	}                    
}

END {
	if( error ) {
		print " ** " m4p_count " protected files found!"
		exit 1
	}

	print "Now copying " files_count " from " src_path_radical " to " out_dir

	for( idx in files_array) {
		src_filename = files_array[idx]
		dst_rel_path = relative_path(src_filename)
		dst_filename = (out_dir "/" dst_rel_path)
		dst_dirname = dirname(dst_filename)

		printf(" [%4d / %d] (%02d\%) ", idx, files_count, 100*idx/files_count)
		print dst_rel_path

		# ensure the directory hierarchy exists
		md_cmd = "mkdir -p \"" dst_dirname "\""
		if(DEBUG) print " ** MKDIR: " md_cmd
		system(md_cmd)

		# copy source file
		cp_cmd = "cp -u \"" src_filename "\" \"" dst_dirname "\""
		if(DEBUG) print " ** CP: " cp_cmd
		system(cp_cmd)
	}
}

# convert a WINDOWS-style path into UNIX-style path
function cygwin_path(file) {
	if( match(file, /^[a-zA-Z]:\\/) ) {
		gsub("\\\\", "/", file)
		drive = ("/" tolower(substr(file,1,1)))
		sub("^[a-zA-Z]:", drive, file)
	}
	return file

	# using cygpath is too slow...
	# cmd="cygpath \"" file "\""
	# cmd | getline result
	# close(cmd)
}

# remove the path radical to get the relative location of the album/title folder hierarchy 
function relative_path(file) {
	return substr(file, length(src_path_radical) + 1);
}

function basename(file) {
 	n = split(file, a, "/")
    return a[n]
}

function dirname(file) {
	# remove end string that does not contains / and finish with an alphanumeric extension
	sub(/[^\/]*\.[a-zA-Z0-9]+$/, "", file)
	return file
}
