#!/usr/bin/bash

# Copyleft (c) Pierre-Jean Turpeau 02/2022 
# <pierrejean AT turpeau DOT net>

# 1st step: verify that the playlist has no protected files (.m4p)
# 2nd step: copy file from the source side to the destination side (only those
#           that have been modified)
# 3nd step: delete file from the destination side that are not defined in the
#           source side         <== not implemented

M3U_FILE=$1
if [ "$M3U_FILE" = "" ]; then
    echo " ** Missing file name"
    echo
    echo "Usage:"
    echo "  $0 <file.m3u>"
    exit 1
elif [ ! -f "$M3U_FILE" ]; then
    echo "$M3U_FILE does not exists"
    exit 1
fi

PATH_RADICAL="/c/Users/${USERNAME}/Music/iTunes/iTunes Media/Music/"

DEBUG=0

BASEDIR=$(dirname "$0")

M3U_NAME=${M3U_FILE%%.*}
OUT_DIR=$PWD/$M3U_NAME

rm -rf "$OUT_DIR"   # TODO: update rather than full rebuild
mkdir "$OUT_DIR"

$BASEDIR/playlist-mirror.awk -v out_dir="$OUT_DIR" -v src_path_radical="$PATH_RADICAL" -v DEBUG=$DEBUG $M3U_FILE
