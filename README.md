# Playlist Mirroring

Update a music folder of a device based on a .m3u playlist.

The goal is to keep in sync (or mirrors) the entire music folder of the
targeted device, which means it copies new files, updates modified files and
deleted undefined files.

_Note: it does not build a .m3u that can be imported into your favorite music player._


# Support of MTP devices

Currently, the script does not support MTP devices such as Android mobile
phones.

In this case, I only use ``playlist-mirror.sh`` to update an intermediate
folder.


<!-- try [FreeFileSync](https://freefilesync.org/) to synchronize the MTP device. -->

<!-- It does the job, but MTP devices support is on my long term roadmap
thanks to [mtp-tools](https://github.com/jabezwinston/mtp-tools) -->

# Details

Developed under Windows 10
 - GNU Awk 5.0.0
 - Bash 4.4.23
